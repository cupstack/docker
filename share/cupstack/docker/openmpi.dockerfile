# ------------------------------------------------------------------------------
# SPDX-License-Identifier: "Apache-2.0"
# Copyright (C) 2021-2022, Jayesh Badwaik <j.badwaik@fz-juelich.de>
# ------------------------------------------------------------------------------
FROM rockylinux:8
ARG COMPILER
ARG MPI
COPY opt/cupstack/ /usr/local
RUN echo "/usr/local/lib64" > /etc/ld.so.conf.d/compiler.conf
RUN echo "/usr/local/lib" >> /etc/ld.so.conf.d/compiler.conf
RUN echo "/usr/local/lib/x86_64-unknown-linux-gnu/" >> /etc/ld.so.conf.d/compiler.conf
RUN ldconfig
