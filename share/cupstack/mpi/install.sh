#!/bin/bash
#-------------------------------------------------------------------------------
# SPDX-License-Identifier: "Apache-2.0"
# Copyright (C) 2020, Jayesh Badwaik <j.badwaik@fz-juelich.de>
#-------------------------------------------------------------------------------
set -euo pipefail

MAJOR_VERSION=4.1
VERSION=4.1.4

SOURCE="${BASH_SOURCE[0]}"
# resolve $SOURCE until the file is no longer a symlink
while [ -h "$SOURCE" ]; do
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  # if $SOURCE was a relative symlink, we need to resolve it relative to the
  # path where the symlink file was located
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
done
SCRIPT_DIRECTORY="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
SHARE_DIRECTORY=$SCRIPT_DIRECTORY/../share/cupstack/docker/

print_help() {
  printf "%6s\n" "usage:"
  printf "    %-30s %-20s\n"  "build --help" "Display this help"
  printf "    %-30s %-20s\n"  "build <args>" "Docker Container to Build"

  echo "Build Arguments:"
  printf "  %-40s %-30s\n"  "-c|--compiler" "Compiler"
}

if [[ $# -eq 0 ]]; then
  print_help
  exit 1
fi

COMPILER_SPECIFIED="false"
MPI_SPECIFIED="false"

while [[ $# -gt 0 ]]; do
  KEY=$1
  case $KEY in
    --compiler)
      shift
      if [[ $# -eq 0 ]]; then
        break
      fi
      COMPILER=$1
      COMPILER_SPECIFIED="true"
      shift
      ;;
    --mpi)
      shift
      if [[ $# -eq 0 ]]; then
        break
      fi
      MPI=$1
      MPI_SPECIFIED="true"
      shift
      ;;
    *)
      echo "Unrecognized Option $KEY"
      print_help
      exit 1
  esac
done

PRINT_HELP_AND_EXIT="false"

if [[ $COMPILER_SPECIFIED == "false" ]]; then
  echo "Missing Compiler."
  PRINT_HELP_AND_EXIT="true"
fi

if [[ $MPI_SPECIFIED == "false" ]]; then
  echo "Missing MPI."
  PRINT_HELP_AND_EXIT="true"
fi

if [[ $PRINT_HELP_AND_EXIT == "true" ]]; then
  print_help
  exit 1
fi

if [[ $MPI == "openmpi" ]]; then
  BUILD_ROOT=$(mktemp -d -t openmpi-XXXXXXXX)

  SOURCE_URL_BASE=https://download.open-mpi.org/release/open-mpi
  SOURCE_URL=$SOURCE_URL_BASE/v$MAJOR_VERSION/openmpi-$VERSION.tar.gz

  pushd $BUILD_ROOT
  mkdir download
  pushd download
  wget $SOURCE_URL
  popd
  mkdir src
  pushd src
  tar -xf ../download/openmpi-$VERSION.tar.gz
  popd
  mkdir build
  pushd build
  if [[ $COMPILER == "llvm" ]]; then
    ../src/openmpi-$VERSION/configure --disable-mpi-fortran \
      CXX=clang++ CC=clang FC=flang

  else
    ../src/openmpi-$VERSION/configure --disable-mpi-fortran
  fi
  make -j4
  make install
  popd
  popd

  ldconfig
  rm -rf $BUILD_ROOT
fi
